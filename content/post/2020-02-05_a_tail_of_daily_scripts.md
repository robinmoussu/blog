﻿---
title: A tail of daily scripts
date: 2020-02-05
tags: ["git", "shell", "scripts"]
---


{{% post-header img="/images/script/main-img.jpg" source="https://pxhere.com/fr/photo/642257 [CC0 license]]" %}}
Writing scripts can help you in a lot of ways. If you don't already do it, you should try to semi-automatize your work.
{{% /post-header  %}}
<!--more-->

Whenever I have a repetitive task to do, I try to automate it. Since I know it will be a one-time task, I don't try to write the most re-usable script, but just something that get the job done.

What I do in a script is very varied. It can be the use of macro in vim to quickly modify a lot of files. It can be some search-and-replace. It can be a test script to find a regression. Each day is a new adventure!

---

Recently, I received a pull request that contained code that was hard to read. It used a lot of abbreviations, and didn't use the same name for the objects than the one used in the rest of the code. So I renamed a lot of words in that new file. I also cleaned-up a bit the interface.

Later that day, the original author send me an update with new things. He added a lot of comments to document its interfaces, and moved a few things around. Unfortunately, we both modified quite heavily the whole document, so merging would have been complicated.

<center>
{{% figure src="/images/script/original_version.png" alt="before using a script" width="50%" %}}
</center>

Instead of manually re-doing all the work I previously did on top of their modifications, I instead opted for a more automatic process.

---

First of all, I had to split my work in two. The first part would contain everything that can be automated (like renaming things, removing trailing whitespace, …). The second should be as small as possible, and will contains things that can't be automated easily (like changing the order of the arguments in a function).

So I re-started from the original state, And recorded everything I did:

{{< code "script.sh" >}}
rename() {
    sed "s/\b$1\b/$2/gI" path/to/the/file && git commit -m "Rename $1 as $2"
}
remove_trailing_space() {
    sed "s/\s\+$//" path/to/the/file &&_git commit -m "Remove trailing spaces"
}

remove_trailing_space
rename abrt_thg abreviated_thing
rename cmprs compress
rename i_foo interface_bar
{{< /code >}}

To be honest, I run a bunch of commands, then looked at my history to be able to put them in a script.

At that point, the only thing that was missing was the manual transformation. There are many ways to do it with git. You can do it with `git reset`, but what is even simpler is to just do a simple `git checkout my_original_transformations .` in the root directory of your project. The "`.`" means that you are asking git to modify "`.`" (the current directory) to match the content of the branch `my_original_transformations`.

To sum up:

```text
 git checkout -b my_transformations my_original_transformations

 vim script.sh
 sh script.sh
 git branch after_running_the_script

 git checkout my_original_transformations '.'
 git commit -m 'Manual transformations'
```

---

Now, it was time to extract what they really did. The big advantage of having a script is that you can re-run it easily.

```text
 git checkout -b their_transformations their_original_transformations
 sh script.sh
```

At that point, I had a clean state. If I wanted to review only the modifications they did, but using the better naming conventions, I only had to do a simple diff.

```text
 git diff after_running_the_script their_transformations
```

And now it was time to merge the two version. Fortunately, it was quite easy, because most of the work I did was automated, and I could only have merge issue in the part of the code I manually transformed.

```text
 git merge my_transformations
```

*Et voilà*!

---

<center>
{{% figure src="/images/script/using_a_script.png" alt="new workflow when using a scrip:" %}}
</center>

---

![Creative Commons License](/images/cc_by.png)
