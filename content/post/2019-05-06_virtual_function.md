﻿---
title: How virtual functions works?
date: 2019-05-06
tags: ["C++"]
---

{{% post-header img="/images/virtual_function/main-img.jpg" source="https://pixnio.com/nature-landscapes/snow/frost-winter-nature-snow-ice-crystal-snowflake-sphere-sky [CC0 license]]" %}}
Do you know how virtual functions works? Let's try to re-implement them.
{{% /post-header  %}}
<!--more-->

In the following snipped, witch one of `Mother::subFunction()` or `Child::subFunction()` is called?

```C++
class Mother {
public:
    void mainFunction() {
        subFunction();
    }
    void subFunction();
};

class Child: public Mother {
public:
    void subFunction();
};

void use() {
    Child c;
    c.mainFunction();
}
```


{{< spoiler text="Generated assembly" >}} 
It's <code>Child::subFunction()</code>.
<br/>
<br/>
<pre><code class="language-objdump-nasm hljs css"><span class="hljs-tag">use</span>():
        <span class="hljs-tag">sub</span>     <span class="hljs-tag">rsp</span>, 24
        <span class="hljs-tag">mov</span>     <span class="hljs-tag">QWORD</span> <span class="hljs-tag">PTR</span> <span class="hljs-attr_selector">[rsp+8]</span>, OFFSET</span> <span class="hljs-attribute">FLAT</span>:<span class="hljs-value">_ZTV5Child</span>+<span class="hljs-number">16</span>
        <span class="hljs-tag">lea</span>     <span class="hljs-tag">rdi</span>, [<span class="hljs-tag">rsp</span>+<span class="hljs-number">8</span>]
        <span class="hljs-tag">call</span>    Child::<span class="hljs-function">subFunction</span>()
        <span class="hljs-tag">add</span>     <span class="hljs-tag">rsp</span>, <span class="hljs-number">24</span>
        <span class="hljs-tag">ret</span>
</code></pre>

I invite you to test it on <a href="https://www.godbolt.org/z/uFpeKI">compiler explorer</a>.
{{< /spoiler >}}

If what you see in the assembly isn't the function you were expected, it's probably because you don't understand yet what a virtual function is. The call to `c.mainFunction()`, is resolved to `Mother::mainFunction()`. When executing this function, a call to `subFunction()` is made. It will in its turn be resolved to the only function with the right signature, while being visible in `Mother`, witch is `Mother::subFunction()`. Indeed, `Child::subFunction` isn't visible in `Mother`. If you want to be able to call `Child::subFunction()` from `Mother` when the real type of `this` is `Child*`, you need to be able to access it, and it will be done by making it a virtual function.

<iframe width="100%" height="400px" src="https://www.godbolt.org/e#g:!((g:!((g:!((h:codeEditor,i:(j:1,lang:c%2B%2B,source:'class+Mother+%7B%0Apublic:%0A++++void+mainFunction()+%7B%0A++++++++subFunction()%3B%0A++++%7D%0A++++virtual+void+subFunction()%3B%0A%7D%3B%0A%0Aclass+Child:+public+Mother+%7B%0Apublic:%0A++++void+subFunction()+override%3B%0A%7D%3B%0A%0Avoid+use()+%7B%0A++++Child+c%3B%0A++++c.mainFunction()%3B%0A%7D'),l:'5',n:'0',o:'C%2B%2B+source+%231',t:'0')),header:(),k:50,l:'4',n:'0',o:'',s:0,t:'0'),(g:!((h:compiler,i:(compiler:g91,filters:(b:'0',binary:'1',commentOnly:'0',demangle:'0',directives:'0',execute:'1',intel:'0',libraryCode:'1',trim:'1'),lang:c%2B%2B,libs:!(),options:'-O3',source:1),l:'5',n:'0',o:'x86-64+gcc+9.1+(Editor+%231,+Compiler+%231)+C%2B%2B',t:'0')),header:(),k:50,l:'4',n:'0',o:'',s:0,t:'0')),l:'2',n:'0',o:'',t:'0')),version:4"></iframe>

---

If you want to understand how virtual functions works, you need to replicate it with function pointer. For that, you need to create a function pointer in `Mother`, than will points to `Mother::subFunction()` if the dynamic type of the instance you are manipulating is `Mother`, and `Child::subFunction()` if it's a `Child`. You could write a minimal example like this.

<iframe width="100%" height="800px" src="https://www.godbolt.org/e#g:!((g:!((g:!((h:codeEditor,i:(j:1,lang:c%2B%2B,source:'struct+Vtable+%7B%0A++++void+(*subFunction)()%3B+//+function+pointer+to+a+function+taking+no+argument%0A++++//+can+contains+other+function+pointers%0A%7D%3B%0A%0Aclass+Mother+%7B%0Apublic:%0A++++void+mainFunction()+%7B%0A++++++++_vtable.subFunction()%3B%0A++++%7D%0A++++static+void+subFunction()%3B%0A%0Aprotected:+//+note:+the+vtable+must+be+visible+by+the+Child%0A++++Vtable+_vtable+%3D+%7B%26Mother::subFunction%7D%3B%0A%7D%3B%0A%0Aclass+Child:+public+Mother+%7B%0Apublic:%0A++++Child()+%7B%0A++++++++_vtable.subFunction+%3D+%26Child::subFunction%3B+//+update+the+vtable+for+the+Child%0A++++%7D%0A++++static+void+subFunction()%3B%0A%7D%3B%0A%0Avoid+use()+%7B%0A++++Mother+m%3B%0A++++m.mainFunction()%3B+//+calls+m._vtable.subFunction+witch+points+to+Mother::subFunction%0A%0A++++Child+c%3B%0A++++c.mainFunction()%3B+//+calls+c._vtable.subFunction+witch+points+to+Child::subFunction%0A%7D'),l:'5',n:'0',o:'C%2B%2B+source+%231',t:'0')),k:100,l:'4',m:50,n:'0',o:'',s:0,t:'0'),(g:!((h:compiler,i:(compiler:g91,filters:(b:'0',binary:'1',commentOnly:'0',demangle:'0',directives:'0',execute:'1',intel:'0',libraryCode:'1',trim:'1'),lang:c%2B%2B,libs:!(),options:'-O3',source:1),l:'5',n:'0',o:'x86-64+gcc+9.1+(Editor+%231,+Compiler+%231)+C%2B%2B',t:'0')),header:(),l:'4',m:50,n:'0',o:'',s:0,t:'0')),l:'3',n:'0',o:'',t:'0')),version:4"></iframe>

---

Unfortunately, the `this` pointer, isn't passed as implicit argument to `subFunction()`, witch means that we can't use member function. To fix this, the real solution is a bit more complicated.


{{< spoiler text="Emulating virtual function – complete implementation" >}} 
<iframe width="100%" height="800px" src="https://www.godbolt.org/e#z:OYLghAFBqd5QCxAYwPYBMCmBRdBLAF1QCcAaPECAKxAEZSAbAQwDtRkBSAJgCFufSAZ1QBXYskwgA5MmaDBAagCyqAgkzEOAZj4AGAIKCCxEcgIKAagSYAjBpgUcA7Hv0L3CgG6o86BRBU1DRAQACpBERsAMREWMzxUFgBKCCTtHgUAekyFAAUfFgINBSIFAFtMMptigDNY%2BMSOAw8snORWBTRCpjwWRVV1YgU6uIIElgUABwKi4kEm/WcAEXSFhdkmeWUB4udXSciGPGQQBZbvX3KelhjR8dTHFzOWjwg1PEEAWm1sUIgAfU81jsmG%2BWmwEWi9TGiSSKTSOlanSYDAYJVQUxmxVKFSqGme7mWBK8Pj8kNuDRYqVWBkmxDwniYRVOzQ82QUURICkEkyYEgUmBqNWOeEwcQAnqROsRMEyHIkGOKFIkHEDbPYpsUNvIpb0jLK/KgasqWA5JsU9dY4phiV19QAPOnc6xjZCWYEawEezCBQaOLRLR58LgANl9wRA5Oh4yJOgWdNUmDMmHQLLcHjt5is6swoQUXpz/sD3BDBZB4c0cYMsdc6zkigAwgg8AxU1NDsdtkEhnt4x2TsSmy30A9e6yXvm1SCi45Q2X7EPW%2BkkSJJug5SV1F5vcMuUEFIv0MSieOSZco3dEtSq/o6Qy5WmWpnMI6hkYmZ3s9P55hDzO9u%2Brr/O0RjaA2FwjhWIShPCPwQCWh4hBelJpE4KyIuyBDiuanSJJ4GgwhMHwKKaKYpmsaE0osBgQQoIiCJgo5PKeFblFRLRlAAdGU1wUoR14ZOy7SoooXE/mCEKRHx4wKAA7oQyAIJivQEIopRQZGUnRo01ann%2BnA3k%2B3G8dpVIIoJbQogwijIJx4k/MhhFyQpSnTCpakYohmlQpeLAUUsUhJIw0gAKxSKQLDSLo4WoNIDb8Pw3KiOIDjcFotDhQQUWBUF6hMFgxCUEFADWIAhbowVSAALOFZRlRVkVSNFpCxVI4WCCAFVZU1gWkHAsBIGgZSTC2GjkJQQ0jfYhXAAAnPQwoMLMHUQDY2WkDYvRMMQ4rSBlpBDRUhQAPIsIq61YDxbD2BdeAyvE%2BEdT1pAvkmIjMlI%2B0qZgDDrUcNjENt4oNhgkifZl9J1eDQXMGwKAJbwjB4DYHWQEFqCTIRT2fMdWjtclEh0DDoXhY1zWtfaAAcIafCGVUKMAyBurNnG0P4uCEFyaX0AeqDDaNPZcOlSQHgjPCZdlcKkHlBVFZVNWkHV5URetrXtZ1pDddFUulbQugVb9Uh4yrz1q5rktBfhczjCAVVAA"></iframe>
{{< /spoiler >}}

![Creative Commons License](/images/cc_by.png)
